/**
 * Copyright (c) 2018
 *
 * This is where the global javascript functions will be kept for use in the website.
 *
 * @author Andrew Pankow <apankow1234@gmail.com>
 * @file app
 * @summary The site's javascript runtime functions 
 */

function main() {
    setRandomTheme();
}