/**
 * Copyright (c) 2018
 *
 * This is a case-study attempt to better my front end skills. 
 * I will be using Spotify for this particular study.
 * I may end up taking this the full mile, but for now, just an attempt to recreate the UI with my style.
 * Also, I don't have a Spotify account, so I've asked my roommate to screenshot their web-app; this is what I'm basing on.
 * I am also adding some functionality for testing the front end, such as ability to turn on/off the CSS and the ability to dynamically switch themes.
 * 
 *
 * @author Andrew Pankow <apankow1234@gmail.com>
 * @file snippets
 * @summary The site's front end interaction functions 
 */


/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*                       Custom Vanilla Snippets                          */
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/


/**
 * Get which prefix is necessary for the browser
 * @return {string} The proper prefix for css effects to work in the current browser.
 */
function getBrowserRenderer()
{
	var prefixes = ['-o-', '-ms-', '-moz-', '-webkit-',''];
	let windowArea = document.createElement("div");
	windowArea.style.background = "";
	for (let i in prefixes)
	{
		windowArea.style.background = prefixes[i]+'linear-gradient(top, #000000, #333333)';
		if (windowArea.style.background && windowArea.style.background!="") {
			return prefixes[i];
		}
	}
}

/**
 * Toggle a checkbox
 * @param {string} ID - The element ID of the checkbox from the HTML page.
 */
function toggle(ID) {
	let checkbox = document.getElementById(ID);
	console.log(this.innerHTML)
	checkbox.checked = !checkbox.checked;
}

/**
 * Cycle through radio buttons.
 * @param {string} groupName - The element name of the radio button group.
 */
function cycle(groupName) {
	let radioGrp = document.getElementsByName(groupName);
	let current = 0;
	for( i in radioGrp ) {
		if(radioGrp[i].checked) {
			current = i;
			radioGrp[i].checked = false;
			break;
		}
	}
	radioGrp[(parseInt(current)+1)%radioGrp.length].checked = true;
}

/**
 * Click link
 * @param {string} ID - The element ID of the <a> from the HTML page.
 */
function doClick(ID) {
	document.getElementById(ID).click();
}

/**
 * Toggle stylesheets on and off
 */
function toggleCSS() {
	for ( i in document.styleSheets ) {
		let css = document.styleSheets.item(i);
   		void(css.disabled = !css.disabled);
   	}
}

/**
 * Get a random color in RGB format
 * @return {object} The color.
 */
function randomRGBColor() {
	return {
		r: Math.floor(Math.random()*256),
		g: Math.floor(Math.random()*256),
		b: Math.floor(Math.random()*256)
	};
}
/**
 * Get a random color in Hexadecimal format
 * @return {string} The color.
 */
function randomHexColor() {
	let color = randomRGBColor();
	return rgbToHex(color.r, color.g, color.b);
}

/**
 * Set theme inside of "window" with a gradient from a color to dark grey
 * @param {string} colorHex - The main color of the gradient.
 */
function setTheme( colorHex )
{
    let windowArea = document.getElementById('window');
	let prefix = getBrowserRenderer();
    windowArea.style.background = prefix+'linear-gradient(top, '+colorHex+', #333333)';
    // windowArea.style.backgroundImage = prefix+'linear-gradient(top, '+colorHex+', #333333)';
}

/**
 * Set theme inside of "window" with a gradient from a random color to dark grey
 */
function setRandomTheme() {
	setTheme(randomHexColor());
}


/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*                         Downloaded Functions                           */
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/
/*------------------------------------------------------------------------*/


/**
 * Color format conversion - RGB to Hexadecimal
 * @author https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
 * @param {number} r - The value of the red channel (from 0 to 255).
 * @param {number} g - The value of the green channel (from 0 to 255).
 * @param {number} b - The value of the blue channel (from 0 to 255).
 * @return {string} The hexadecimal version of the color.
 */
function rgbToHex( r, g, b ) {
    /**/
    function extract( component ) {
        let hex = component.toString(16);
        /* add a "0" to keep at least 2 digits */
        return (hex.length == 1) ? "0" + hex : hex;
    }
    return "#" + extract(r) + extract(g) + extract(b);
}

/**
 * Color format conversion - Hexadecimal to RGB
 * @author https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
 * @param {string} hex - The hexadecimal value of the color (with or without the "#").
 * @return {object} The color.
 */
function hexToRgb( hex ) {
    /* ([a-f\d]{2}) searches for pairs of 0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f */
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec( hex );
    return result ? {
        r: parseInt( result[1], 16 ),
        g: parseInt( result[2], 16 ),
        b: parseInt( result[3], 16 )
    } : null;
}