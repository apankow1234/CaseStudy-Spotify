# CaseStudy-Spotify

## Control Panel

[Live Version](https://apankow1234.github.io/CaseStudy-Spotify/control_panel.html)

### With CSS

![Site with Styles](screenshots/screencapture-control_panel-2018-05-28-16_28_50.png "with CSS")

### Original Spotify

![Real Spotify CP](screenshots/screencapture-SPOTIFY-control_panel.png "Original Spotify")

### Bare HTML -> NO CSS

![Site without Styles](screenshots/screencapture-control_panel-bare_HTML.png "without CSS")